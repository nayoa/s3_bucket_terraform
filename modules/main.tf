data "aws_caller_identity" "current" {}
data "aws_region" "current" {}

resource "aws_s3_bucket" "s3_bucket" {
  bucket = "${var.s3_bucket_name}"
  acl    = "private"
  region = "${data.aws_region.current.name}"

  versioning {
    enabled = true
  }

  tags {
    Name = "${var.s3_bucket_name}"
  }
}
