# AWS Simple Storage Service (S3) Terraform module

Terraform module which creates S3 buckets in AWS

These types of resources are supported:

* [S3](https://www.terraform.io/docs/providers/aws/r/s3_bucket.html)

Root module calls these modules which can also be used separately to create independent resources:

## Usage

```hcl
module "s3-bucket" {
  source = "./modules"

  s3_bucket_name = "s3-example"
}
```

## Examples

* [Complete S3 example](https://github.com/nayoa/s3_terraform_module/blob/master/main.tf)


## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| s3_bucket_name | Name of S3 bucket | string | - | yes |

## Outputs

| Name | Description |
|------|-------------|
| S3_bucket_arn | The ARN of the S3 Bucket |

## Authors

Module managed by [Nayo Akinyele](https://github.com/nayoa).
