package test

import (
	"fmt"
	"strings"
	"testing"

	"github.com/gruntwork-io/terratest/modules/random"
	"github.com/gruntwork-io/terratest/modules/terraform"
)

func TestTerraformAwsS3Bucket(t *testing.T) {
	t.Parallel()

	// Give this S3 Bucket a unique ID for a name tag so we can distinguish it from any other S3 bucket's running
	expectedName := fmt.Sprintf("terratest-aws-s3-%s", strings.ToLower(random.UniqueId()))
	awsRegion := "eu-west-1"
	// tagName := "Name"

	terraformOptions := &terraform.Options{
		// The path to where our Terraform code is located
		TerraformDir: "./modules",

		// Variables to pass to our Terraform code using -var options
		// "username" and "password" should not be passed from here in a production scenario.
		Vars: map[string]interface{}{
			"s3_bucket_name":     expectedName,
			"s3_prevent_destroy": false,
		},

		// Environment variables to set when running Terraform
		EnvVars: map[string]string{
			"AWS_DEFAULT_REGION": awsRegion,
		},
	}

	// At the end of the test, run `terraform destroy` to clean up any resources that were created
	defer terraform.Destroy(t, terraformOptions)

	// This will run `terraform init` and `terraform apply` and fail the test if there are any errors
	terraform.InitAndApply(t, terraformOptions)

	// Run `terraform output` to get the value of an output variable
	terraform.Output(t, terraformOptions, "s3_bucket_arn")

	// aws.AddTagsToResource(t, awsRegion, s3BucketArn, map[string]string{"testing": "testing-tag-value"})

	// s3Tags := aws.FindS3BucketWithTag(t, awsRegion, tagName, s3BucketArn)

	// testingTag, containsTestingTag := s3Tags["testing"]
	// assert.True(t, containsTestingTag)
	// assert.Equal(t, "testing-tag-value", testingTag)

	// // Verify that our expected name tag is one of the tags
	// nameTag, containsNameTag := s3Tags["Name"]
	// assert.True(t, containsNameTag)
	// assert.Equal(t, expectedName, nameTag)

}
