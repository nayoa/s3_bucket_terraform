output "s3_bucket_arn" {
  value = "${module.s3-eks-terraform-state.s3_bucket_arn}"
}
