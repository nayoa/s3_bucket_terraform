######
# S3
######
module "s3-eks-terraform-state" {
  source = "./modules"

  s3_bucket_name = "nayoa-eks-terraform-state"
}
